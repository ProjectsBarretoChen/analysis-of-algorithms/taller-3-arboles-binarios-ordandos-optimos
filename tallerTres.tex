\documentclass[oneside,spanish]{amsart}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{amstext}
\usepackage{amsthm}
\usepackage{amssymb}
\PassOptionsToPackage{normalem}{ulem}
\usepackage{ulem}
\usepackage{graphicx, color}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Some new commands
\newcommand{\noun}[1]{\textsc{#1}}
\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algoritmo}
\floatname{algorithm}{\protect\algorithmname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\theoremstyle{definition}
\newtheorem*{defn*}{\protect\definitionname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{xmpmulti}
\usepackage{algorithm,algpseudocode}

\makeatother

\usepackage{babel}
\addto\shorthandsspanish{\spanishdeactivate{~<>}}

\providecommand{\definitionname}{Definición}

\begin{document}
\title{Taller 3: Árboles binarios ordenados óptimos}
\author{Juan Sebastián Barreto Jiménez $^1$ \hspace{1cm} Janet Chen He$^1$}
\date{\today	
\\
$^1$Departamento de Ingeniería de Sistemas, Pontificia Universidad           Javeriana\\Bogotá,  Colombia \\
      \texttt{\{juan\_barreto, j-chen\}@javeriana.edu.co}\\~\\}
\begin{abstract}
En este documento se presenta el desarrollo del taller 3 que consta de escribir formalmente e implementar un algoritmo basado en la estrategia  ``programación dinamica'', para resolver el problema de construir un árbol binario ordenado óptimo  (ABOP) a partir de los bits de un archivo.
\end{abstract}
\maketitle

\part{Análisis y diseño del problema}

\section{Análisis}
Para construir un ABOP, es importante tener en cuenta los siguientes aspectos.

\begin{enumerate}
\item Recibir por línea de comandos el nombre de un archivo y un tamaño de elemento ("byte" o "word").
\item Realizar el árbol a partir de los bits del archivo interpretados como bytes o como words.
\item Informar el tamaño del mensaje.
\item Frecuencia y profundidad del elemento más frecuente.
\item Frecuencia y profundidad del elemento menos frecuente.
\item Tiempo promedio de búsqueda (100 búsquedas) del elemento más frecuente.
\item Tiempo promedio de búsqueda (100 búsquedas) del elemento menos.
\end{enumerate}

\subsection{Formulación de la ecuación}
Sabiendo lo anterior tenemos entonces que la ecuación sería la siguiente: \\ \\
\[
e[i,j]=\begin{Bmatrix}
q_{i-1} & ;j = i-1 \\ 
min_{i\leq r\leq j} \{ e [i, r - 1] + e [r + 1, j] + w (i, j)\} & ;i\leq j
\end{Bmatrix}
\]
\[
w(i,j)=\sum_{l=i}^{j} pl + \sum_{l=i-1}^{j} ql
\]

\newpage
\section{Diseño}

\begin{defn*}
Entradas:
\begin{enumerate}
\item $P_{i} \in \mathbb R$ $P_{i} \wedge > 0$ $P_{i} \wedge < 1$, secuencia de elementos, que pertenece a los reales y su valor es menor a cero y mayor a 1. Representa la probabilidad de éxito.
\item $Q_{i} \in \mathbb R$ $Q_{i} \wedge > 0$ $Q_{i} \wedge < 1$, secuencia de elementos, que pertenece a los reales y su valor es menor a cero y mayor a 1. Representa la probabilidad de error.
\item $i \in \mathbb N$, valor de la posición inicial, que pertenece a los naturales.
\item $j \in \mathbb N$, valor de la posición final, que pertenece a los naturales.
\end{enumerate}
\end{defn*}
~~~~
\begin{defn*}
Salidas: \\
Se define una tupla con las siguientes variables:
\begin{enumerate}
\item $q \in \mathbb R$, valor mínimo esperado.
\end{enumerate}
\end{defn*}
~~~~~

\part{Algoritmos}

\section{Ejercicio}

\subsection{Algoritmo inocente}

Este algoritmo inocente, consta de presentar una solución recurrente. Por lo que se describe el algoritmo para la contrucción de un árbol ABOP.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{BuildOptTreeAux}{$P,Q,i,j$}
    \If{$j = i - 1$}
        \State\Return{$Q[i-1]$}
    \Else
        \State$q\leftarrow inf$
        \State$r\leftarrow i$
        \For{$r \in j$}
            \State$el\leftarrow \Call{BuildOptTreeAux}{P,Q,i,r-1}$
            \State$er\leftarrow \Call{BuildOptTreeAux}{P,Q,1,j}$
            \State$w\leftarrow Q[i-1]$
            \State$l\leftarrow i$
            
            \For{$l \in j$}
            \State$w\leftarrow P[l] + Q[l] + w$
            \EndFor
            
        \State$e\leftarrow el + er + w$
        \If{$e < q$}
         \State$q\leftarrow e$
        \EndIf
        \EndFor
    \EndIf

\State\Return{$q$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Inocente}
\end{algorithm}

\newpage

\subsection{Algoritmo Memoización}

Este algoritmo de memoización consta de reemplazar todo los retornos por un tabla que almacena el dato necesario, haciendo que no sea necesario calcular el valor de un elemento ya analizado. En el caso de este ejercicio, se concluye que es una tabla de dos dimensiones y se puede inicializar con ceros.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{BuildOptTreeAux}{$P,Q,i,j$}\\
    \textbf{let} M[i][j] tabla de memoización
    \If{$j = i - 1$}
        \State$M[i,j]\leftarrow Q[i-1]$
    \Else
        \State$q\leftarrow inf$
        \State$r\leftarrow i$
        \For{$r \in j$}
            \State$el\leftarrow \Call{BuildOptTreeAux}{P,Q,i,r-1,M}$
            \State$er\leftarrow \Call{BuildOptTreeAux}{P,Q,1,j,M}$
            \State$w\leftarrow Q[i-1]$
            \State$l\leftarrow i$
            
            \For{$l \in j$}
            \State$w\leftarrow P[l] + Q[l] + w$
            \EndFor
            
        \State$e\leftarrow el + er + w$
        \If{$e < q$}
         \State$q\leftarrow e$
        \EndIf
        \EndFor
    \EndIf

\State$M[i,j]\leftarrow q$

\State\Return{$M[i,j]$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Memoización}
\end{algorithm}

\subsection{Algoritmo Botton-up}

El botton-up permite pasar lo anterior a interactivo, ya que como se podrá evidenciar en la implementación, la solución inocente presenta muchos llamados a retorno haciendo que no sea viable.

\subsection{Algoritmo Botton-up con Backtracking}

Finalmente para backtracking, se usa otra tabla adicional que permite guardar los datos necesarios para obtener la solución.

\part{Conclusión}
¿Un ABOP garantiza búsquedas más rápidas que un AVL o un Rojo-Negro?

Se concluye que un ABOP no siempre garantiza una búsqueda más rápida que un AVL o Rojo-Negro, sin embargo, se espera que las mayoría de veces si, puesto que un árbol Rojo-Negro no garantiza el minimizar el número de nodos visitados. Haciendo que el proceso sea más lento, mientras que con un árbol ABOP implementado con programación dinámica busca solucionar este problema.

\end{document}