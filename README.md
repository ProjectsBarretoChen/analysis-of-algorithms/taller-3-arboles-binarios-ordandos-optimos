# **TALLER 3 - ÁRBOLES BINARIOS ORDENADOS ÓPTIMOS** <br/>

## Presentado por :star2:

* Juan Sebastián Barreto Jiménez
* Janet Chen He

## Objetivo
Escribir formalmente e implementar un algoritmo basado en la estrategia "programacion dinamica".

## Descripción
El problema a trabajar en este taller es: construir un árbol binario ordenado óptimo  (ABOP) a partir de los bits de un archivo.

Para esto, usted debe crear un programa en C++ que:

1. Reciba por línea de comandos el nombre de un archivo y un tamaño de elemento ("byte" o "word").
2. Construya el ABOP a partir de los bits del archivo interpretados como bytes o como words.
3. Informe por pantalla:
    - Tamaño del mensaje.
    - Frecuencia y profundidad del elemento más frecuente.
    - Frecuencia y profundidad del elemento menos frecuente.
    - Tiempo promedio de búsqueda (100 búsquedas) del elemento más frecuente.
    - Tiempo promedio de búsqueda (100 búsquedas) del elemento menos.

## Estructura de Archivos
1. main.cpp -> Contiene la implementación de problema de programación dinámica
2. output.txt -> Contiene el histograma
3. tallerTres.tex -> Contine el documento en .tex
4. Taller_3__Arboles_binarios_ordenados_optimos.pdf -> Contine el documento en .pdf