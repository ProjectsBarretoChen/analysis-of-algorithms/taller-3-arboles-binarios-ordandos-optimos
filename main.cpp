#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

template<class _T>
void process(const std::string&);
double BuildOptTree(
    const std::vector< double > &,
    const std::vector< double > &
);
double BuildOptTreeAux(
    const std::vector< double > &,
    const std::vector< double > &,
    unsigned long long, unsigned long long,
    std::vector<std::vector<double>> &
);

int main( int argc, char **argv){

    if( argc < 3){
        std::cerr << "Usage: " << argv[0] << " input byte/word" << std::endl;
        return -1;
    } // end if

    std::string fname = argv[1];
    std::string type = argv[2];

    if ( type == "byte"){
        process< unsigned char >( fname);
    } else if ( type == "word") {
        process< unsigned short >( fname);
    } // end if

    return 0;
} // end main fun

template< class _T >
void process( const std::string &fname){

    std::ifstream fstr( fname.c_str(), std::ios::binary| std::ios::ate );
    unsigned long long size = fstr.tellg();
    fstr.seekg( 0, std::ios::beg );

    std::vector< _T > data(size / sizeof( _T) );
    char *buffer = reinterpret_cast< char* >( data.data() );
    fstr.read( buffer, data.size() * sizeof( _T ));
    fstr.close();

    // Build histogram
    std::vector< double > P( 1 << (sizeof( _T ) << 3), 0);
    for( unsigned long long i = 0; i < data.size(); ++i ) {
        P[ data[ i ] ] += double( 1 ) / double( data.size() );
    } // end for

    std::vector < double > Q(P.size() + 1, 0);

    // for( unsigned long long i = 0; i < P.size(); ++i ) {
    //     std::cout << i << " " << P[ i ] << std::endl;
    // } // end for

    std::cout << BuildOptTree( P, Q) << std::endl;

} // end process fun template

double BuildOptTree(
    const std::vector< double > &P,
    const std::vector< double > &Q
){
    std::vector<std::vector<double>> M(P.size(), std::vector<double>(Q.size(), -1.0));
    std::cout << "BuildOptTree" << std::endl;
    return BuildOptTreeAux( P, Q, 1, P.size(), M );
} // end BuildOptTree fun

// Inocente
// double BuildOptTreeAux(
//     const std::vector< double > &P,
//     const std::vector< double > &Q,
//     unsigned long long i, unsigned long long j
// ){
//     std::cout << "BuildOptTreeAux" << std::endl;
//     if ( j == i - 1){
//         return Q[i-1];
//     } else {
//         double q = std::numeric_limits< double >::max();

//         for ( unsigned long long r = i; r <= j; ++r ) {
//             double el = BuildOptTreeAux(P, Q, i, r-1);
//             double er = BuildOptTreeAux(P, Q, r+1, j);
//             double w = Q[ i-1 ];

//             for ( unsigned long long l = i; l <= j; l++){
//                 w += P[ l ] + Q[ l ];
//             }
//             double e = el + er + w;
//             if( e < q){
//                 q = e;
//             }
//         } // end for
//         return q;
//     } // end if 
// } // end BuildOptTreeAux fun 

// Memorización
double BuildOptTreeAux(
    const std::vector< double > &P,
    const std::vector< double > &Q,
    unsigned long long i, unsigned long long j,
    std::vector<std::vector<double>> &M
){
    std::cout << "BuildOptTreeAux" << std::endl;
    if ( j == i - 1){
        M[i][i] = Q[i-1];
    } else {
        double q = std::numeric_limits< double >::max();

        for ( unsigned long long r = i; r <= j; ++r ) {
            double el = BuildOptTreeAux(P, Q, i, r-1, M);
            double er = BuildOptTreeAux(P, Q, r+1, j, M);
            double w = Q[ i-1 ];

            for ( unsigned long long l = i; l <= j; l++){
                w += P[ l ] + Q[ l ];
            }
            double e = el + er + w;
            if( e < q){
                q = e;
            }
        } // end for
        M[i-1][j] = q;
    } // end if 
    return M[i][j];
} // end BuildOptTreeAux fun
